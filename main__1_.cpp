#include <vector>
#include <list>
#include <string>
#include <cmath>
#include <iostream>

using namespace std;

template <typename Key, typename Value>
class HashTable {
    unsigned capacity = 50;
    unsigned size = 0;
    vector<list<pair<Key, Value>>> table;

public:
    HashTable () {
        table = vector<list<pair<Key, Value>>>(100);
    }

    ~HashTable() {
        table.clear();
    }

    void Insert(const Key& key, const Value & val){

        size_t pos = HashFunc(key);

        pair <Key, Value> pair (key, val);
        table[pos].push_back(pair);

    }

    size_t HashFunc (const string & key) {
        unsigned int p = 131;
        int M = 43;

        unsigned long hash = 0;

        for (int i = 0; i < key.size(); i++) {
            hash += key[i]*(unsigned long)pow(p,i) % M;
        }
        hash = hash % M;
        return static_cast <size_t> (hash); // (size_t)hash - cast hash to size_t
    }

    void OutPut() {
        for (const auto& list : table) {
            for (const auto& elem : list) {
                cout << elem.first << ' ' << elem.second<<endl;
            }
        }
    }

    Value Search(const Key & key) {
        size_t pos = HashFunc(key);
        for (const auto& elem: table[pos]) {
            if (elem.first == key) {
                return elem.second;
            }
        }
    }
    void Delete(const Key & key) {
        size_t pos = HashFunc(key);
        int i = 0;
        auto it = table[pos].begin();
        for (const auto& elem: table[pos]) {
            if (elem.first == key) {
                cout << "Was Deleted by key :" << elem.first << endl;
                advance(it, i);
                table[pos].erase(it);
                break;
            }
            i +=1;

        }
    }

    const Value operator[](const Key& key){
        size_t pos = HashFunc(key);
        for (const auto& elem: table[pos]) {
            if (elem.first == key) {
                return elem.second;
            }
        }
    }

};

int main ()
{
    HashTable <string, string> names;
    names.Insert("Hamilton", "Lewis");
    names.Insert("Verstapen", "Max");
    names.Insert("Russel", "George");
    names.OutPut();
    cout << "Was found by key :" << names.Search("Verstapen") << endl;
    names.Delete("Russel");
    names.OutPut();
    cout << names["Hamilton"] << endl;
    return 0;

}